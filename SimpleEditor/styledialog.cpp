#include "styledialog.h"
#include "ui_styledialog.h"
#include "tools.h"

StyleDialog::StyleDialog(QWidget *parent) :
    QDialog(parent, Qt::WindowTitleHint | Qt::WindowSystemMenuHint | Qt::WindowCloseButtonHint),
    ui(new Ui::StyleDialog)
{
    ui->setupUi(this);
    
    // load index of last used theme
    int index = Settings::loadSettings("combobox", "index").toInt();
    
    // set loaded index to combobox
    ui->comboBox->setCurrentIndex(index);
}

StyleDialog::~StyleDialog() {
    delete ui;
}

// getter of currentStyle variable
QString StyleDialog::getCurrentStyle() const {
    return currentStyle;
}

// on button in buttonBox clicked
void StyleDialog::on_buttonBox_clicked(QAbstractButton *button)
{
    // get the button clicked in buttonBox
    QDialogButtonBox::StandardButton stdButton = ui->buttonBox->standardButton(button);
	
    // handling it
    if (stdButton == QDialogButtonBox::Ok) {
        // get the chosen index of theme
        int currentTheme = ui->comboBox->currentIndex();
        
        // read the chosen theme according to index
        if (currentTheme == DEFAULT_THEME) {
            currentStyle = readTextFile("://css/styles/DefaultTheme.css");            
        } else if (currentTheme == DARK_THEME) {
            currentStyle = readTextFile("://css/styles/DarkTheme.css");  
        }
        
        // save into setting about last chosen theme
        Settings::saveSettings("combobox", "index", QString::number(ui->comboBox->currentIndex()));
        
        // emit signal "ok pressed in button box" - "accepted"
        accept();
    }  else if (stdButton == QDialogButtonBox::Cancel) {
        // emit signal "cancel pressed in button box" - "rejected"
        reject();
    }
}
