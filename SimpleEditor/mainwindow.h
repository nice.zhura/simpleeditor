#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT
    
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    
private slots:
    // on action About Qt triggered in menu bar
    void on_actionAbout_Qt_triggered();
    
    // on action About triggered in menu bar
    void on_actionAbout_triggered();
    
    // quitting application
    void quitApp();
    
    // on action Quit triggered in menu bar 
    void on_actionQuit_triggered();
    
    // on action Copy triggered in menu bar
    void on_actionCopy_triggered();
    
    // on action Cut triggered in menu bar
    void on_actionCut_triggered();
    
    // on action Paste triggered in menu bar
    void on_actionPaste_triggered();
    
    // on action Undo triggered in menu bar
    void on_actionUndo_triggered();
    
    // on action Redo triggered in menu bar
    void on_actionRedo_triggered();
    
    // on action Font triggered in menu bar
    void on_actionFont_triggered();
    
    // on action Style triggered in menu bar
    void on_actionStyle_triggered();
    
    // on action Open triggered in menu bar
    void on_actionOpen_triggered();
    
    // on action Save As triggered in menu bar
    void on_actionSave_As_triggered();
    
    // on action Save triggered in menu bar
    void on_actionSave_triggered();
    
private:
    Ui::MainWindow *ui;
    
    // path to last opened file
    QString file;
};
#endif // MAINWINDOW_H
