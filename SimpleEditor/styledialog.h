#ifndef STYLEDIALOG_H
#define STYLEDIALOG_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class StyleDialog;
}

class StyleDialog : public QDialog {
    Q_OBJECT
    
public:
    explicit StyleDialog(QWidget *parent = nullptr);
    ~StyleDialog();
    
    // getter of currentStyle variable
    QString getCurrentStyle() const;
    
private slots:
    // on button in buttonBox clicked
    void on_buttonBox_clicked(QAbstractButton *button);
    
private:
    Ui::StyleDialog *ui;
    
    // last chosen style in combobox
    QString currentStyle;
    
    // enum of added styles
    enum themes { DEFAULT_THEME, DARK_THEME };
};

#endif // STYLEDIALOG_H
