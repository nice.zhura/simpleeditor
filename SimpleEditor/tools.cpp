#include "tools.h"

#include <QFile>
#include <QSettings>
#include <QTextStream>

// reading text from file
QString readTextFile(QString path) {
    // get the result variable for text
    QString result;
    
    // get the file variable
    QFile file(path);
    
    // read text from file using TextSteam
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);
        result = in.readAll();
    }
    file.close();
        
    // return text from file
    return result;
}

// writing text into file
void writeTextFile(QString path, QString value)
{
    // get the file variable
    QFile file(path);
    
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return;
    }
    
    // write text into file using TextSteam
    QTextStream out(&file);
    out << value;
    
    file.close();
}

// saving settings by key, group key
void Settings::saveSettings(QString groupKey, QString key, QString value)
{
    QSettings settings("MySoft", "SimpleEditor");
    
    settings.beginGroup(groupKey);
    settings.setValue(key, value);
    settings.endGroup();
}

// loading settings by key, group key
QString Settings::loadSettings(QString groupKey, QString key)
{
    QString value;
    QSettings settings("MySoft", "SimpleEditor");
    
    settings.beginGroup(groupKey);
    value = settings.value(key, QVariant(0)).toString();
    settings.endGroup();
    
    return value;
}
