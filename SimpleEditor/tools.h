#ifndef TOOLS_H
#define TOOLS_H

#include <QString>

// reading text from file
QString readTextFile(QString path);

// writing text into file
void writeTextFile(QString path, QString value);

class Settings {
public:
    // saving settings by key, group key
    static void saveSettings(QString groupKey, QString key, QString value);
    
    // loading settings by key, group key
    static QString loadSettings(QString groupKey, QString key);
    
};

#endif // TOOLS_H
