#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "styledialog.h"
#include "aboutdialog.h"
#include "tools.h"

#include <QApplication>
#include <QFontDialog>
#include <QFileDialog>
#include <QTimer> 
#include <QFile>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    
    // load last selected theme
    QString css = Settings::loadSettings("textedit", "css");
    
    // load last selected font
    QFont font;
    font.fromString(Settings::loadSettings("textedit", "font"));
  
    // set the last style and last font
    ui->textEdit->setStyleSheet(css);
    ui->textEdit->setFont(font);
    
    // load path of last opened file
    file = Settings::loadSettings("textedit", "file");
    
    // read text from path
    QString plainText = readTextFile(file);
    
    if (plainText.isEmpty()) {
        // if: file is empty clear settings
        QFile::remove(file);
        file.clear();
    } else {
        //else: set text to textEdit
        ui->textEdit->clear();
        ui->textEdit->setPlainText(plainText);
    }
}

MainWindow::~MainWindow() {
    // save path of last opened file
    Settings::saveSettings("textedit", "file", file);
    delete ui;
}

// on action About Qt triggered in menu bar
void MainWindow::on_actionAbout_Qt_triggered() {
    // get about Qt dialog
    QApplication::aboutQt();
}

// on action About triggered in menu bar
void MainWindow::on_actionAbout_triggered() {
    // get the style settings for about dialog
    QString css = readTextFile("://css/about.css");
    
    // create new dialog
    AboutDialog* aboutDialog = new AboutDialog(this);
    
    // set the style to dialog
    if (!css.isEmpty()) {
        aboutDialog->setStyleSheet(css);
    }
    
    // show the modal dialog to user
    aboutDialog->exec();
}

// quitting application
void MainWindow::quitApp() {
    QApplication::quit();
}

// on action Quit triggered in menu bar
void MainWindow::on_actionQuit_triggered() {
    // write message into status bar about quitting the application
    ui->statusbar->showMessage("App will be killed in 2 sec ...");
    
    // using a timer kill application in 2 sec 
    QTimer::singleShot(2000, this, SLOT(quitApp()));    
}

// on action Copy triggered in menu bar
void MainWindow::on_actionCopy_triggered() {
    // calling copy method of texEdit
    ui->textEdit->copy();
}

// on action Cut triggered in menu bar
void MainWindow::on_actionCut_triggered() {
    // calling cut method of texEdit
    ui->textEdit->cut();
}

// on action Paste triggered in menu bar
void MainWindow::on_actionPaste_triggered() {
    // calling paste method of texEdit
    ui->textEdit->paste();
}

// on action Undo triggered in menu bar
void MainWindow::on_actionUndo_triggered() {
    // calling undo method of texEdit
    ui->textEdit->undo();
}

// on action Redo triggered in menu bar
void MainWindow::on_actionRedo_triggered() {
    // calling redo method of texEdit
    ui->textEdit->redo();
}

// on action Font triggered in menu bar
void MainWindow::on_actionFont_triggered() {
    // get initial font from textEdit
    QFont initialTextEditFont = ui->textEdit->font();
    
    // check if font is correct
    bool isFontCorrect;
    
    // get the font dialog
    QFont textEditFont = QFontDialog::getFont(
                    &isFontCorrect, initialTextEditFont, this);
    
    // set the font to textEdit
    if (isFontCorrect) {
        ui->textEdit->setFont(textEditFont);
        
        // save last chosen font
        Settings::saveSettings("textedit", "font", textEditFont.toString());
    }
}

// on action Style triggered in menu bar
void MainWindow::on_actionStyle_triggered() {
    // create new style dialog
    StyleDialog* styledialog = new StyleDialog(this);
    
    // show the modal dialog to user
    int returned = styledialog->exec();
    
    // if "ok - pressed button"
    if (returned == StyleDialog::Accepted) {
        // get chosen style 
        QString css = styledialog->getCurrentStyle();     
        
        // set chosen style
        if (!css.isEmpty()) {
            ui->textEdit->setStyleSheet(css);
            
            // save last chosen style
            Settings::saveSettings("textedit", "css", css);
        }
    }
}

// on action Open triggered in menu bar
void MainWindow::on_actionOpen_triggered() {
    // stirng for file text
    QString fileContent;
    
    // get the path from get open file name dialog 
    QString filename = QFileDialog::getOpenFileName(this, "Open File", QDir::currentPath(), tr("Text files (*.txt)"));

    if (filename.isEmpty()) {
        return;
    }
    
    // set path of last opened file into variable
    file = filename;
    
    // read text from file
    fileContent = readTextFile(filename);
    
    // set text to textEdit
    ui->textEdit->clear();
    ui->textEdit->setPlainText(fileContent);
}

// on action Save As triggered in menu bar
void MainWindow::on_actionSave_As_triggered() {
    // get the path from get save file name dialog 
    QString filename = QFileDialog::getSaveFileName(this, "Save As", QDir::currentPath(), tr("Text files (*.txt)"));
    
    if (filename.isEmpty()) {
        return;
    }
    
    // set path of last opened file into variable
    file = filename;
    
    // write text into file
    writeTextFile(filename, ui->textEdit->toPlainText());
}

// on action Save triggered in menu bar
void MainWindow::on_actionSave_triggered() {
    if (file.isEmpty()) {
        // if text int textEdit is empty 
        // call on save as trigered method
        on_actionSave_As_triggered();
    } else {
        // else write text into file
        writeTextFile(file, ui->textEdit->toPlainText());
    }
}
